
window.paymentCallback = null;

angular.module('payment', [])


.directive('autoSubmitForm', ['$timeout', 'autoSubmitFormDelegate', function($timeout, autoSubmitFormDelegate) {
    return {
        replace: true,
        scope: {},
        template: '<form action="{{formData.redirectUrl}}" method="{{formData.redirectMethod}}">'+
                      '<div ng-repeat="(key,val) in formData.redirectData">'+
                           '<input type="hidden" name="{{key}}" value="{{val}}" />'+
                      '</div>'+
                  '</form>',
        link: function($scope, element, $attrs) {
            
            autoSubmitFormDelegate.submitCallback = function(data) {
                $scope.formData = data;
                $timeout(function() {
                    element[0].submit();
                });
             };
        }
    }
}])

.factory('autoSubmitFormDelegate', function(){
    var delegate = {};
    
    delegate.submitCallback = null;
    
    delegate.submitForm = function(data){
        if(delegate.submitCallback){
            delegate.submitCallback(data);
        }
    }
    
    return delegate;
})

.controller('PaymentCtrl', function($scope, $timeout, $window, $sce, autoSubmitFormDelegate){


    $window.paymentCallback = function(data){
        console.log("callback called");
        data.redirectUrl = $sce.trustAsResourceUrl(data.redirectUrl);    
        $timeout(function(){
            autoSubmitFormDelegate.submitForm(data);
        });
    };
    
})