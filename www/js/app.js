// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic'])

.run(function($ionicPlatform,$rootScope) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });


  $rootScope.$on("$locationChangeStart", function(event, next, current) { 
        // handle route changes     
        console.log('URL ',current);
        console.log('HI');
    });


})

app.controller('ctrl',function($http,$scope ){
  console.log(window.location.host);

  $http({
    // Enter your api endpoint for encrypting form data
    url:'http://mywebsite.com/api/ccavEncryptData',
    method:'post',
    data:'order_id=100',
    headers:{ 'Content-type' : 'application/x-www-form-urlencoded' }

  }).success(function(data){
      console.log(data);
      $scope.encRequest = data.response.encRequest

  }).error(function(error){
    console.log(error);
  })






})
